<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
    	return $this->belongsTo('\App\User');
    }

    //belongs to many links this order to the products table via the products_orders pivot table
    //withPivot contains all the  columns that are not foreign keys and are not id's nor timestamps in the pivot table
    	//we can separate multiple columns in the pivot table that are not ids or foreign keys or timestamps with comma
    //withTimestamps automatically populate the timestamps as soon as an entry for products_orders(pivot table) is created. 
    //the table doesnt need to be specified if and only if the ff criteria are satisfied:
        //1. the name of the pivot table should have components that are SINGULAR
        //2. the order of the related table in the pivot are in ALPHABETICAL order
        //eg. order_product
        //belongsToMany("App\\Product")->withPivot...
    public function products(){
    	return $this->belongsToMany('\App\Product', 'products_orders')->withPivot('quantity', 'subtotal')->withTimeStamps();
    }

    public function status(){
    	return $this->belongsTo('\App\Status');
    }
}
