<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category(){
    	return $this->belongsTo('\App\Category');// <-- this establishes the many to one relationship of products to a category. The naming convention is singular
    }

    public function orders(){
    	return $this->belongsToMany('\App\Order', 'products_orders')->withPivot('quantity', 'subtotal')->withTimeStamps();
    }
}
