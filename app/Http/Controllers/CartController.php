<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product; // allows us to use the Product model
use Auth; //allows us to call the user Session

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // Session::pull('cart');
        // dd(Session::get('cart'));
        //use the session cart to get the details for the item
        $details_of_item_in_cart = [];
        $total = 0;
        if(Session::exists('cart') || Session::get('cart') !=null){
        foreach(Session::get('cart') as $item_id => $quantity){
            //because session cart has keys(item_id) and values (quantity)

            //1. find the item --->>> use App\Product;
            $product = Product::find($item_id);
            //2. get the details needed (add properties not in the original item)
            $product->quantity = $quantity;
            $product->subtotal = $product->price * $quantity;

            //note these properties (quantity and subtotal are not part of product stored in the database, they are only for $product)

            //3. push to array containing the details
            //syntax: array_push(target_array, data to be pushed)
            array_push($details_of_item_in_cart, $product);
            $total += $product->subtotal;
            }
        }

        //4. send the array to the view 
        

        return view ('products.cart', compact("details_of_item_in_cart", "total"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        // $cart[$request->item_id] = $request->quantity; //when an item is added to cart, it will assisn the quantity to index number equal to the iten_id
        // // ways to create array
        // // $cart = array(values);
        // // $cart = [value1, value2 ..]

        // //To use session:
        // Session::put('cart', $cart); //adds a session variable called cart with the content from $cart
        // // dd(Session::get('cart'));

        //this is only halfway done, why? because the Session::put overwrites the original content. Find a way to prevent it from overwriting. (i.e. revamp the entire logic)


        //1. check if we already have a cart
        //1a. if there is no cart, create a new one
        //1b. if there is already a cart, call the cart and update the content.
        //2b. save the updated cart in the session

        $cart = []; //empty cart

        // dd($cart);

        if(Session::exists('cart')){ //if there is a cart in our session, pull it.
            $cart = Session::get('cart');
        }

        if(array_key_exists($request->item_id, $cart)){
            $cart[$request->item_id] += $request->quantity;
        }else{
        $cart[$request->item_id] = $request->quantity; //updates the cart
        }
        // dd($cart);
        Session::put('cart', $cart); // push it back to the session cart
        Session::flash('message', $request->quantity .'items added to cart');
        return redirect ('/products'); // simply returns to the catalogue page
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Session::get("cart");
        $cart[$id] = $request->item_id;
        
        Session::put("cart", $cart);
        return redirect('/cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $cart = Session::get('cart');
        // unset($cart[$id]);
        // Session::put('cart', $cart);

        

        Session::forget("cart.$id");// similar to uset($_Session['cart'][$id]);

        $product = Product::find($id);

        Session::flash('message', $product->name.' item deleted');

        return redirect('/cart');
    }

    public function emptyCart(){
        if (Session::exists('cart')){
        //remove all cart entries
        Session::forget('cart');
    }
    return redirect('/cart');

    }

    public function confirmOrder(){
        //check if the user is logged in
        //if the user is logged in, get the cart again, recalculate the subtotal and total and send it to a view called orders.confirm
        //if the user is not logged in, redirect user to the login page.

        if(Auth::check()){
            $details_of_item_in_cart = [];
            $total = 0;
            if(Session::exists("cart") || Session::get("cart") !=null){
                foreach (Session::get("cart") as $item_id => $quantity){
                    $product = Product::find($item_id);
                    //add quantities and subtotals, compute the total and push the product into details_of_item_in_cart
                    $product->quantity = $quantity;
                    $product->subtotal = $product->price * $quantity;
                    array_push($details_of_item_in_cart, $product);
                    $total += $product->subtotal;
            
                }
            }
                return view("orders.confirm", compact("details_of_item_in_cart", "total"));

        }else{
            return redirect('/login');
        }
    }
}
