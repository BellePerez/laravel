<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Auth; //for user
use App\Product; //use the product model
use App\Stattus; //use the status model
use Session;//to get the cart

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check()){// only shows for those who are logged in
            $orders =[];

            if(Auth::user()->isAdmin){
                $orders = Order::all(); //gets the list of all orders
            } else{
                $orders = Order::where("user_id", Auth::user()->id)->get();
            //find is used for primary keys
            }
            return view("orders.orderlist", compact("orders"));

        }else{

            
            return redirect("/login");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //1. create a new order with total = 0
        $refNo = "B49".time();
        $new_order = new Order;
        $new_order->refNo = $refNo;
        $new_order->user_id = Auth::user()->id;
        $new_order->status_id= 1; //always pending (default)
        $new_order->total= 0;

        // dd($new_order);//to test of the user id is corect as well as the refNo is updated
        $new_order->save(); //saves to db

        //2. attach the products to the order-- meaning we are going to write to the products_orders table
        $total =0;
        foreach(Session::get("cart") as $item_id => $quantity){
            $product =Product::find($item_id); // we need this to get the proce property
            $subtotal = $product->price * $quantity;
            //check list to attach : order_id ($new_order), item_id ($item_id), quantity ($quantity), subtotal ($subtotal)
            //to populate the products_order table
            $new_order->products()->attach($item_id, ["quantity" => $quantity, "subtotal" => $subtotal]);// translates to - for this order, get the pivot table and create a new row with its order_id, item_id and the quantity and the subtotal fields
            //the second parameter of attach containes all the pivot columns
            //update the total
            $total += $subtotal;

        }
        //3. update the new order's total
        $new_order->total =$total;
        $new_order->save();
        Session::forget("cart");
        // return "order successfully created";
        return view("orders.results", compact("refNo"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->status_id = 2;
        $order->save();
        return redirect("/orders");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->status_id = 3;
        $order->save();
        return redirect("/orders");
    }
}
