<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Product;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    //     $categories = Category::all();
    //     return view('products.create', compact('categories'));
    // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->name);
        //instantiation- act of creating an object from a class
        //1 instantiate a new object  from the category model
        $category = new Category;
        
        //2 set the property values of the newly instantiated object accrodingly

        $category->name = $request->name;
        //3 save the newly instantiated object, doing so will store it as an entry in its corrwsponding table in the DB
        $category->save();
       

        Session::flash('newcategory', $request->name .' category added');

        return redirect('products/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) 
    {
        // no need to use find(id) as resource already does it for us and is put into $category // dd($category);
        //to get the items rekated to the category, we use the products property og the category, this only works if there is an established realtionship in the model.

        // dd($category->products);

        $products = $category->products;
        return view('products.catalogue', compact('products'));
    

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
