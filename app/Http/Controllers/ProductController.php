<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Category;
use Session;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //get all the product from the database
        $products = Product::all(); // we use the model to pull all products
        return view('products.catalogue', compact('products')); //catalog.blase.php in views /products which corresp[onds to $products
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // retrieve all category via the all() method of the category model and return to the products creation page
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->category);

        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->category_id = $request->category;

        //manage image upload

        $image = $request->file('image');
        //actual file name when stored in product directory
        //date and time concatinated with . , concat with the file extension
        $image_name = time().".".$image->getClientOriginalExtension();
        //specify the subdirectory within the project's public folder where file will be saved
        $destination = "images/";
        //perform the actual saving of the image file by passing in the target directory and the desired file name to the move() method of the $image variable
        $image->move($destination, $image_name);
        //set the relative link pointing to the location of the image asset as the value for the product's img_path property
        $product->img_path = $destination.$image_name;
        //saves this product in the satabe
        $product->save();

        Session::flash('message', $request->name .' added to products');

        return redirect ('/products');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //use this function to have a view showing the details of the item in a form. (edit.blade.php)
        //create the form in edit.blade.php
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->category_id = $request->category;
        
        //if no new image uploaded
        if($request->file('image') != null){
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $product->img_path = $destination.$image_name;
        }


        $product->save();

        Session::flash('message', $request->name .' edit successful!');

        return redirect('/products/'.$product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // dd($product);
        // $product->name = $request->name;

        $product->delete();

        // Session::flash('message', $request->name .' deleted!');

        return redirect("/products"); //we simply route back to the catalogue
    }
}
