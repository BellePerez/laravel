<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function products(){
    	//if we called the products() method of a category object, we would be able to pull ALL the products for that category and we can also pull their perspective properties
    	//from parent to child is ALWAYS PLURAL
    	return $this->hasMany('\App\Product');
    }
}
