<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// }); //--- default from laravel

Route::get('/', 'ProductController@index');

Route::resource('categories', 'CategoryController');
// /categories, /categories/{id}
Route::resource('products', 'ProductController', ["except" => ["show", "index"]])->middleware("isAdmin");
//the routes in this resource except for show and index will use the isAdmin middleware

Route::resource('products', 'ProductController', ["only" => ["show", "index"]]);
// use the routes show and index only of this resources, without a middleware
Route::resource('statuses', 'StatusController');
Route::resource('orders', 'OrderController');
//route to empty cart
Route::delete('/cart/empty', 'CartController@emptyCart');
//route to confirm the order after check out
Route::get("cart/confirm", "CartController@confirmOrder");
Route::resource('cart', 'CartController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
