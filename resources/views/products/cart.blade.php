@extends('layouts.app')
@section('content')


<div class="container">

	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<h2>My Cart</h2>
			<table class="table table-striped">
				<thead>
					<th>Name</th>
					<th>Decription</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
				</thead>
				<tbody>
					@if(!empty($details_of_item_in_cart))
					@foreach($details_of_item_in_cart as $indiv_product)
					<tr>
						<td>{{$indiv_product->name}}</td>
						<td>{{$indiv_product->description}}</td>
						<td>{{$indiv_product->price}}</td>
						{{-- <td>
						<form action="/cart/" method="POST">
							@csrf

							<button type="submit">-</button>
						</form>{{$indiv_product->quantity}}
						<form action="/cart" method="POST">
							@csrf
							<button type="submit">+</button></form>
						</td> --}}
						<td>
							{{-- {{$indiv_product->quantity}} --}}
						<form action="/cart/{{$indiv_product->id}}" method="POST">
						@csrf
						{{method_field("PATCH")}}
						<input type="number" name="item_id" value="{{$indiv_product->quantity}}" min="1" class="form-control">
						<button type="submit" class="btn btn success">Edit Quantity</button>
						</form>
						
						</td>

						<td>PHP{{$indiv_product->subtotal}}</td>
						
						<td>
							<form action="/cart/{{$indiv_product->id}}" method="POST"> 
								@csrf
								{{method_field("DELETE")}}
								<button type="submit" class="btn btn-danger">Remove</button>
							</form>
						</td>
					</tr>
					
					@endforeach
					<div class="row">
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td>PHP{{$total}}</td>
						</tr>
					</div>
					@else
					<tr>
						<td colspan="5">No Items to Display</td>
					</tr>
					@endif
				</tbody>
			</table>
			{{-- <a href="/cart/confirm" class="btn btn-success">Checkout</a> --}}
			<div class="col-lg-8 offset-lg-2">
			@if(Session::has('message'))
		    <h5>{{Session::get('message')}}</h5> @endif
		    </div>

		</div>
		<div class="col-lg-8 offset-lg-2">
			<form action="/cart/empty" method="POST">
				@csrf
				{{method_field("DELETE")}}
				<button type="submit" class="btn btn-danger">Empty Cart</button>
			</form>
		</div>
		
			<div class="col-lg-8 offset-lg-2 mt-2">
				<a href="/cart/confirm" class="btn btn-success">Proceed to Check out</a>
				<a href="/products" class="btn btn-info">Add More Items</a>
			</div>
		
	</div>
</div>
@endsection