@extends('layouts.app')
@section('content')

{{-- <div class="row">
	<div class="col-lg-6">
		<a href="/cart" class="btn btn-secondary">View cart</a>
	</div>
</div> --}}

	<a href="/products" class="btn btn-primary">All</a>
	
	@foreach(App\Category::all() as $indiv_category)
		
		<a href="/categories/{{$indiv_category->id}}" class="btn btn-primary">{{$indiv_category->name}}</a>
	{{-- this uses the category route (/categories) and needs the show() method of the CategoryController --}}
	@endforeach
	<div class="row">
		@if(Session::has('message'))
		<h4>{{Session::get('message')}}</h4> @endif
	</div>

	<div class="row">
		
		@foreach($products as $indiv_product)
		
		<div class="col-lg-4">
			
			<div class="card">
				<img class="card-img-top" src="{{asset($indiv_product->img_path)}}" alt="Card image cap" style="margin-top:10px; height:300px; object-fit:contain;">

				<div class="card-body">
					<div class="card-title">
						<h5> Name: {{$indiv_product->name}}</h5>
					</div>

					<div class="card-text">Description : {{$indiv_product->description}}
					</div>

					<div class="card-text">Price : {{$indiv_product->price}}
					</div>

					<div class="card-footer">
					<a href="/products/{{$indiv_product->id}}" class="btn btn-secondary">View Details</a>
					</div>
					@if(!Auth::check() || !Auth::user()->isAdmin)
					<div class="card-footer">
					<form action="/cart" method="POST">
						@csrf
						<input type="hidden" name="item_id" value="{{$indiv_product->id}}">
						<input type="number" class="form-control" name="quantity" min="1" value="1">
						<button type="submit" class="btn btn success">Add To Cart</button>
					</form>

					</div>
					@endif

				</div>
			</div>
		</div>
		@endforeach

	</div>

@endsection