@extends('layouts.app')

@section('content')
<div class="row">
		@if(Session::has('message'))
		<h4>{{Session::get('message')}}</h4> @endif
</div>

	<div class="row">
		<div class="col-lg-4 offset-lg-4">
		<div class="card my-5">
			<img class="card-img-top" src="{{asset($product->img_path)}}" alt="card">

			<div class="card-body text-center">
				<div class="card-title">
					<h5> Name:{{$product->name}}</h5>
				</div>
				<div class="card-text">Description: {{$product->description}}</div>
				<div class="card-text">Price: {{$product->price}} </div>
			</div>
			<h3> The category of this product is {{$product->category->name}}</h3> {{-- $product->category pulls ALL the properties of the category --}}

			<div class="card-footer">

				@if(Auth::user()->isAdmin)

				<a href="/products/{{$product->id}}/edit" class="btn btn-secondary justify-content-center">Edit Product</a>
				@endif

				@if(Auth::user()->isAdmin)
				<form action="/products/{{$product->id}}" method="POST">
					@csrf
					{{method_field("DELETE")}} {{-- POST and GET are the HTTP verbs that are supported by method, we override it to become delete --}}
					<button type="submit" class="btn btn-danger">Delete</button>
					{{-- <div class="row">
						@if(Session::has('message'))
						<h4>{{Session::get('message')}}</h4> @endif
					</div> --}}
				</form>
				@endif

			</div>
		</div>
	</div>
	</div>
@endsection