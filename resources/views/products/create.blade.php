@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-lg-8 offset-lg-2 pt-4">
		<form method="POST" action="/categories">
			@csrf
			<div class="form-group">
				<label form="name">Name</label>
				<input type="text" name="name" id="name" class="form-control">
			</div>
			<button type="submit" class="btn btn-success">
				Add New Category
			</button>
		</form>

		@if(Session::has('message'))
		<h4>{{Session::get('message')}}</h4> @endif
	</div>

</div>

<div class="row">
		@if(Session::has('newcategory'))
		<h4>{{Session::get('newcategory')}}</h4> @endif
</div>

<div class="row">
	<div class="col-lg-8 offset-lg-2">
		<form method="POST" action="/products" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" name="name" id="name" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Description</label>
				<textarea name="description" id="description" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="price">Price</label>
				<input type="number" name="price" id="price" step=0.01 min=0 class="form-control">
			</div>
			<div class="form-group">
				<label for="image">Upload Image</label>
				<input type="file" name="image" id="image" class="form-control">
			</div>
			<div class="form-group">
				<select name="category" class="form-control">
					@foreach($categories as $category)
					<option value="{{$category->id}}">{{$category->name}}</option>
					@endforeach
				</select>
				<button type="submit" class="btn btn-success">Add New Product</button>
			</div>
		</form>
	</div>
</div>
@endsection