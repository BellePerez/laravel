@extends('layouts.app')

@section('content')

<h1>Edit Product</h1>
	<form action="/products/{{$product->id}}" method="POST" enctype="multipart/form-data">

		@csrf
		{{method_field("PUT")}}
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" class="form-control" value="{{$product->name}}">
		</div>

		<div class="form-group">
			<label for="description">Description</label>
			<textarea name="description" class="form-control">{{$product->description}}</textarea>
		</div>
		<div class="form-group">
			<label for="price">Price</label>
			<input type="number" name="price" class="form-control" step=0.01 min=0" value="{{$product->price}}">
		</div>
		<div class="form-group">
					<img src="{{asset($product->img_path)}}">
					<label for="image">Upload Image</label>
					<input type="file" name="image" id="image" class="form-control">
		</div>
		<div class="form-group">
			<select name="category">
				@foreach(App\Category::all() as $category)
				<option value="{{$category->id}}"
					{{$product->category_id == $category->id ? "selected" : ""}}> {{-- ternary operator statement similar top an if else staetement but is a single line and only outputs value. syntax would be : condition ? value if true : value of false --}}
					{{$category->name}}</option>@endforeach
			</select>
		</div>
		<button type="submit">Edit </button>
	</form>
		
@endsection