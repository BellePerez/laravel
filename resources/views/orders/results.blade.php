@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<h3> Your Order {{$refNo}} has been successfully created!</h3>

		</div>
		<a href="/products" class="btn btn-info">Back to Catalogue</a>
	</div>
</div>

@endsection