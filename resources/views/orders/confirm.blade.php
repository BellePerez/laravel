@extends('layouts.app')
@section('content')

<div class="row">
	<div class="col-lg-8 offset-lg-2">
		<h2>Order Confirmation</h2>
		<table class="table table-striped">
			<thead>
					<th>Name</th>
					<th>Decription</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
			</thead>
			<tbody>
			@foreach($details_of_item_in_cart as $indiv_product)
				<tr>
						<td>{{$indiv_product->name}}</td>
						<td>{{$indiv_product->description}}</td>
						<td>{{$indiv_product->price}}</td>
						<td>{{$indiv_product->quantity}}</td>
						<td>{{$indiv_product->subtotal}}</td>
				</tr>
			@endforeach
			<div class="row">
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td>Total</td>
					<td>PHP{{$total}}</td>
				</tr>

			</div>
			</tbody>
			
		</table>
		<form action="/orders" method="POST">
			@csrf
			<button type="submit" class="btn btn-success">Confirm Order</button>
		</form>
	</div>
</div>


@endsection