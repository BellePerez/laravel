<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');//product name
            $table->text('description');/// since it will be acommodating longer text, use text instead of 'string'
            $table->decimal('price', 10, 2); // we only use float if we are keen on high precision value, the 10 is the digit, 2 the decimal places
            $table->string('img_path');
            $table->boolean('isActive')->default(true);//so it will not be completely deleted
            $table->unsignedBigInteger('category_id');//this is the foreign key since the relation ship is one (category) to many (products)
            $table->timestamps();
            //link the products table to categories table via its foreign key
            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
