<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Admin Two',
        	'email'=> 'admintwo@admin.com',
        	'password' => bcrypt('/8520/8520'),
        	'isAdmin' => 1

        ]);
    }
}
